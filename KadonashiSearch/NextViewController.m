//
//  NextViewController.m
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "NextViewController.h"
#import "AppDelegate.h"
#import "DeviseSize.h"

@interface NextViewController ()<NADViewDelegate>
@property (nonatomic, retain) NADView * nadView;
@end

@implementation NextViewController

- (void)loadView
{
    [super loadView];
    // レイヤーの作成
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    // レイヤーサイズをビューのサイズをそろえる
    gradient.frame = self.view.bounds;
    
    // 開始色と終了色を設定
    gradient.colors = @[
                        // 開始色
                        (id)[UIColor colorWithRed:.87 green:.68 blue:.27 alpha:1].CGColor,
                        // 終了色
                        (id)[UIColor colorWithRed:.89 green:.54 blue:.72 alpha:1].CGColor
                        ];
    
    // レイヤーを追加
    [self.view.layer insertSublayer:gradient atIndex:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.canDisplayBannerAds = true;
    
    int screenHeight = self.view.frame.size.height;
    
    NSString *deviceName = [DeviseSize getNowDisplayDevice];
    CGRect rectTop;
    CGRect rectBottom;
    CGRect requestBackBottom;
    CGRect rectBackBottom;
    UILabel *secondLabel = [[UILabel alloc] init];
    
    //iPhone4s
    if([deviceName isEqual:@"iPhone4s"]){
        rectTop = CGRectMake(0, 30, 320, 45);
        rectBottom = CGRectMake(0, screenHeight-95, 320, 45);
        requestBackBottom = CGRectMake(0, screenHeight-145, 320, 45);
        rectBackBottom = CGRectMake(0, screenHeight-230, 310, 45);
        secondLabel.frame = CGRectMake(0, 0, 325, 400);
    //iPhone5またはiPhone5s
    }else if ([deviceName isEqual:@"iPhone5"]){
        rectTop = CGRectMake(0, 30, 320, 45);
        rectBottom = CGRectMake(0, screenHeight-95, 320, 45);
        requestBackBottom = CGRectMake(0, screenHeight-145, 320, 45);
        rectBackBottom = CGRectMake(0, screenHeight-245, 320, 45);
        secondLabel.frame = CGRectMake(0, 10, 320, 500);
    //iPhone6
    }else if ([deviceName isEqual:@"iPhone6"]){
        rectTop = CGRectMake(0, 30, 375, 45);
        rectBottom = CGRectMake(0, screenHeight-95, 375, 45);
        requestBackBottom = CGRectMake(0, screenHeight-175, 365, 45);
        rectBackBottom = CGRectMake(0, screenHeight-285, 375, 45);
        secondLabel.frame = CGRectMake(0, 10, 375, 500);
    //iPhone6 plus
    }else if ([deviceName isEqual:@"iPhone6plus"]){
        rectTop = CGRectMake(0, 30, 414, 45);
        rectBottom = CGRectMake(0, screenHeight-95, 414, 45);
        requestBackBottom = CGRectMake(0, screenHeight-180, 414, 45);
        rectBackBottom = CGRectMake(0, screenHeight-280, 400, 45);
        secondLabel.frame = CGRectMake(0, 45, 414, 500);
    } else {
        rectTop = CGRectMake(65, 30, 650, 45);
        rectBottom = CGRectMake(65, screenHeight-112, 650, 45);
        requestBackBottom = CGRectMake(0, screenHeight-200, 768, 45);
        rectBackBottom = CGRectMake(0, screenHeight-500, 768, 65);
        secondLabel.frame = CGRectMake(0, 70, 768, 500);
    }
    
//    CGRect rectTop = CGRectMake(0, 30, 375, 45);
    UIImageView *imageViewTop = [[UIImageView alloc]initWithFrame:rectTop];
    imageViewTop.image = [UIImage imageNamed:@"ornament.png"];
    [self.view addSubview:imageViewTop];
    
//    CGRect rectBottom = CGRectMake(0, screenHeight-95, 375, 45);
    UIImageView *imageViewBottom = [[UIImageView alloc]initWithFrame:rectBottom];
    imageViewBottom.image = [UIImage imageNamed:@"ornament_r.png"];
    [self.view addSubview:imageViewBottom];
    
//    CGRect requestBackBottom = CGRectMake(0, screenHeight-145, 375, 45);
    UIButton *requestButton = [[UIButton alloc]initWithFrame:requestBackBottom];
    [requestButton setTitleShadowColor:[ UIColor grayColor ] forState:UIControlStateNormal ];
    requestButton.titleLabel.shadowOffset = CGSizeMake( 1, 1 );
    requestButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    requestButton.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    [requestButton setTitle:@"✉️新規角ナシリクエスト" forState:UIControlStateNormal];
    [requestButton addTarget:self action:@selector(mailStartUp)
         forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:requestButton];
    
//    CGRect rectBackBottom = CGRectMake(0, screenHeight-245, 375, 45);
    UIButton *backButton = [[UIButton alloc]initWithFrame:rectBackBottom];
    [backButton setTitleShadowColor:[ UIColor grayColor ] forState:UIControlStateNormal ];
    backButton.titleLabel.shadowOffset = CGSizeMake( 1, 1 );
    [backButton setTitle:@"☜ 戻る" forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToTopPage:)
  forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
    
//    UILabel *secondLabel = [[UILabel alloc] init];
//    secondLabel.frame = CGRectMake(20, 45, 335, 500);
    secondLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.textColor = [UIColor blackColor];
    secondLabel.textAlignment = NSTextAlignmentCenter;
    secondLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    secondLabel.shadowOffset = CGSizeMake(0, 2);
    secondLabel.numberOfLines = 0;
    secondLabel.font = [UIFont fontWithName:@"07LogoTypeGothic7" size:15.0];
    [self.view addSubview:secondLabel];
    
    if ([self.rowSectionString  hasPrefix:@"Hしたくない"]) {
        secondLabel.text = @"膀胱炎になった";
    } else if ([self.rowSectionString  hasPrefix:@"タバコを吸わないでほしい"]) {
        secondLabel.text = @"めっちゃ咳き込む";
    } else if ([self.rowSectionString  hasPrefix:@"二人で遊ぶことを誘われたけど、みんな含めてなら遊んでも良い"]) {
        secondLabel.text = @"タコパしたい！タコパにしようよー！\n\n※人数が要る遊びを提案";
    } else if ([self.rowSectionString  hasPrefix:@"あなたと居ても楽しくない"]) {
        secondLabel.text = @"なんか、斬新な遊びがしたい！\n良いプラン思いついたら教えて！";
    } else if ([self.rowSectionString  hasPrefix:@"いつも自分のことしか考えてないよね"]) {
        secondLabel.text = @"ねー、それ一緒に考えてあげるよ！\nだから私のも一緒に考えてー";
    } else if ([self.rowSectionString  hasPrefix:@"最近奥さんが亡くなった方がいて、娘のように可愛がってくれるのはうれしいけど毎日電話がかかってくるのでやめてほしい"]) {
        secondLabel.text = @"LINEしよ！LINE好きなんだよね、私";
    } else if ([self.rowSectionString  hasPrefix:@"打ち上げなんかいってられるか"]) {
        secondLabel.text = @"さっき昂りすぎて実はちょっとチビッた！\n早くパンツ履き替えたい！今日は帰るね！";
    } else if ([self.rowSectionString  hasPrefix:@"そういう言い方されたら傷つくんですよ"]) {
        secondLabel.text = @"オブラートお願いします";
    } else if ([self.rowSectionString  hasPrefix:@"なんかいつもプラン立てるの私だけなんだけど少し協力してくれない？"]) {
        secondLabel.text = @"「ねー、Aちゃん、これ調べてー\nあ、Bくん、ここ予約してー！」\nって独断で係りを振り分ける。分担。";
    } else if ([self.rowSectionString  hasPrefix:@"会いたくない"]) {
        secondLabel.text = @"ニンニク食べちゃった";
    } else if ([self.rowSectionString  hasPrefix:@"あなたと一緒にいたら精神的に疲れる"]) {
        secondLabel.text = @"あなた元気すぎるから、\n私がもっと元気な時に改めて会おう\n\nor\n\nまじ妖怪メンタルクラッシャーだな";
    } else if ([self.rowSectionString  hasPrefix:@"どこに住んでるとかプライベートなこと教えたくない"]) {
        secondLabel.text = @"それ、デリケートゾーン(　^ω^)";
    } else if ([self.rowSectionString  hasPrefix:@"辛い・帰りたい・辞めたい・面倒臭い"]) {
        secondLabel.text = @"メンタルブレイクしました";
    } else if ([self.rowSectionString  hasPrefix:@"早く帰りたい"]) {
        secondLabel.text = @"今日人と待ち合わせしてるから、\nまた明日ねー！";
    } else if ([self.rowSectionString  hasPrefix:@"なんか重くてしんどくなってきた"]) {
        secondLabel.text = @"一緒に居ても楽しいより辛いの方が増えてきた\nなんか最近、嬉しいより悲しいの方が多い";
    } else if ([self.rowSectionString  hasPrefix:@"後輩からいきなりタメ語で話された"]) {
        secondLabel.text = @"ということは、しっかりする必要も無いから\nとことん甘える";
    } else if ([self.rowSectionString  hasPrefix:@"好きな人が自分の嫌いな人とばっかりしゃべって自分にかまってくれない"]) {
        secondLabel.text = @"構ってくれないというか、\n話に入れてないだけだから、\n自分から話に割って入る「え、何が？」「それでそれで！」「ねー、なにそれ！！」って、\nどんな話題でも参加しまくる";
    } else if ([self.rowSectionString  hasPrefix:@"あんまり関わりたくない友達に毎月「シフト出た？シフト表送って！遊ぼう!」って催促される"]) {
        secondLabel.text = @"「シフト出たんだけど、今月無理そうだ！\nまた来月あたりで調整しよう！」を繰り返す\n三ヶ月程度で、疎遠になり始めると思うよ！";
    } else if ([self.rowSectionString  hasPrefix:@"担任が体を触ってくる"]) {
        secondLabel.text = @"もう二歩下がって\n手が届かない距離から話をしよう";
    } else if ([self.rowSectionString  hasPrefix:@"気が使えないね"]) {
        secondLabel.text = @"自分のことで、\nいっぱいいっぱいに生きてるもんね";
    } else if ([self.rowSectionString  hasPrefix:@"食べ方汚い！"]) {
        secondLabel.text = @"インドと何かあった？元彼インド人？\nあ、なんか食べ方がそんな感じしたから！\n手づかみも慣れ親しんでそうだなって";
    } else if ([self.rowSectionString  hasPrefix:@"もう帰りたい"]) {
        secondLabel.text = @"明日、早いんだよねー";
    } else if ([self.rowSectionString  hasPrefix:@"うるさい"]) {
        secondLabel.text = @"マスク、使う？";
    } else if ([self.rowSectionString  hasPrefix:@"「普通」◯◯でしょ？"]) {
        secondLabel.text = @"異例だね";
    } else if ([self.rowSectionString  hasPrefix:@"生理的に無理"]) {
        secondLabel.text = @"大好きなんだけど、\n家族みたいな感じなんだよね";
    } else if ([self.rowSectionString  hasPrefix:@"顔、怖い！"]) {
        secondLabel.text = @"肝試しやるとき、呼びたい！";
    } else if ([self.rowSectionString  hasPrefix:@"チームワークがなってなくて足手まとい"]) {
        secondLabel.text = @"存在感あり過ぎるから、\nもうちょい馴染む方向で！";
    } else if ([self.rowSectionString  hasPrefix:@"マナーがなってない"]) {
        secondLabel.text = @"もしかして帰国子女だったり？\nなんか感情の起伏が、\n文化レベルで独特だなぁって";
    } else if ([self.rowSectionString  hasPrefix:@"全体的にキツイ"]) {
        secondLabel.text = @"とりあえず、総入れ替えしてみるのが良さそう";
    } else if ([self.rowSectionString  hasPrefix:@"ほんとに無理"]) {
        secondLabel.text = @"今は厳しい";
    } else if ([self.rowSectionString  hasPrefix:@"なんか臭いんだけど"]) {
        secondLabel.text = @"体調崩してるっぽいよ！";
    } else if ([self.rowSectionString  hasPrefix:@"今日はめんどくさい"]) {
        secondLabel.text = @"別日でもいい？";
    } else if ([self.rowSectionString  hasPrefix:@"上司のいいなりすぎて犬みたい"]) {
        secondLabel.text = @"NOと言えない日本人選手権があったら、\nうちの会社の代表で出場してほしい\n絶対勝てる";
    } else if ([self.rowSectionString  hasPrefix:@"おまえ、風呂入れよ！"]) {
        secondLabel.text = @"最後いつお風呂入った？\n\nor\n\n一番風呂、譲るよー！";
    } else if ([self.rowSectionString  hasPrefix:@"飲み物、食べ物をあげたくない(関節キスが嫌)とき"]) {
        secondLabel.text = @"あ、私今風邪ひいてるから、ダメだ！";
    } else if ([self.rowSectionString  hasPrefix:@"めっちゃLINEが長く続いてて、もうやめたい"]) {
        secondLabel.text = @"キラキラした表情のスタンプ";
    } else if ([self.rowSectionString  hasPrefix:@"すんげえきらい"]) {
        secondLabel.text = @"アレルギー出ちゃうんだよね";
    } else if ([self.rowSectionString  hasPrefix:@"食後相手の歯にのりとかがついてる時は？"]) {
        secondLabel.text = @"なんかもっと食べさせる\n沢山食べれば、そのうち取れる";
    } else if ([self.rowSectionString  hasPrefix:@"おもしろくない！！さむい！！"]) {
        secondLabel.text = @"え？ちょっと見逃したかも、もっかいやって？ ";
    } else if ([self.rowSectionString  hasPrefix:@"しったかぶってくるのうざいよ"]) {
        secondLabel.text = @"ね、ほんとに知ってる？\nその情報、信頼できる？\nそれでいく？\nファイナルアンサー？ ";
    } else if ([self.rowSectionString  hasPrefix:@"好きです"]) {
        secondLabel.text = @"グッと来てます ";
    } else if ([self.rowSectionString  hasPrefix:@"人に色々言う割には自分もあまり(仕事が)できてないよね！！"]) {
        secondLabel.text = @"同志ですね！";
    } else if ([self.rowSectionString  hasPrefix:@"うざい"]) {
        secondLabel.text = @"まじここウーザス学園だわー";
    } else if ([self.rowSectionString  hasPrefix:@"人としてダメ"]) {
        secondLabel.text = @"猫だったら、良かったのにね";
    } else if ([self.rowSectionString  hasPrefix:@"性欲高すぎ"]) {
        secondLabel.text = @"いつも大体理性が失われてる状態だからあれだけど、\n通常の性格って、\nどんなんなの？\n会ってみたいんだけど";
    } else if ([self.rowSectionString  hasPrefix:@"汚されそうだしパクられそうだから、漫画貸したくない"]) {
        secondLabel.text = @"これ保管用なんだよね\n読む用は今人に貸してる";
    } else if ([self.rowSectionString  hasPrefix:@"話がやたら長い"]) {
        secondLabel.text = @"要点教えて";
    } else if ([self.rowSectionString  hasPrefix:@"マザコンきめえ"]) {
        secondLabel.text = @"お母さんに勝てる気がしないから、\n私は棄権するね";
    } else if ([self.rowSectionString  hasPrefix:@"鼻毛でてるよ！"]) {
        secondLabel.text = @"「激辛ラーメン食べよー」\n\n↓\n\n「あ、鼻かむ？はい、ティッシュ」";
    } else if ([self.rowSectionString  hasPrefix:@"自慢話ばっかりでうんざり"]) {
        secondLabel.text = @"「武勇伝武勇伝、\n武勇伝デンデデンデン、\nレッツゴー！」\nと煽る";
    } else if ([self.rowSectionString  hasPrefix:@"口、くさい"]) {
        secondLabel.text = @"ちょっと、距離近い。";
    } else if ([self.rowSectionString  hasPrefix:@"調子乗るな"]) {
        secondLabel.text = @"いったん落ち着こう。\nまだリスク対策が不完全だし、ね。";
    } else if ([self.rowSectionString  hasPrefix:@"ただの遊びなんだけど"]) {
        secondLabel.text = @"やー、楽しかったねー、\nまた機会があったら遊ぼうね！";
    } else if ([self.rowSectionString  hasPrefix:@"お金の使い方が下手すぎ"]) {
        secondLabel.text = @"お金、預かろうか？\n買い物、付き合おうか？";
    } else if ([self.rowSectionString  hasPrefix:@"気づいてないと思うけど、あなた結構嫌われてるよ"]) {
        secondLabel.text = @"今のメンバーには\n適合してない感じがあるから、\nコミュニティを変えたほうがいいかも。";
    } else if ([self.rowSectionString  hasPrefix:@"早漏すぎだろお前"]) {
        secondLabel.text = @"もっかい。";
    } else if ([self.rowSectionString  hasPrefix:@"家に入れたくない"]) {
        secondLabel.text = @"犬のしつけがまだ終わってなくて、\n噛むんだよね";
    } else if ([self.rowSectionString  hasPrefix:@"死んだほうがマシだろ"]) {
        secondLabel.text = @"打たれ強いよね";
    } else if ([self.rowSectionString  hasPrefix:@"誘いに対して、正直乗り気じゃない"]) {
        secondLabel.text = @"締め切り仕事があって、\nそれが順調に終われば行けるんだけど、\n難航した場合、無理なんだよね";
    } else if ([self.rowSectionString  hasPrefix:@"恋愛対象では無くなった"]) {
        secondLabel.text = @"今は、もっと、夢のために頑張りたいの";
    } else if ([self.rowSectionString  hasPrefix:@"友達の鼻くそが、誰が見ても見えるくらいでてる"]) {
        secondLabel.text = @"こより使って、\n誰が一番くしゃみを我慢できるか競争しよー！";
    } else if ([self.rowSectionString  hasPrefix:@"あの子とじゃなくて私とデートして！"]) {
        secondLabel.text = @"私も会いたい";
    } else if ([self.rowSectionString  hasPrefix:@"異性に連絡先を聞かれた時の断り方"]) {
        secondLabel.text = @"何用？";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)backToTopPage:(id)sender {
    [self dismissModalViewControllerAnimated:NO];
}

- (void)mailStartUp
{
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    if (mailClass != nil)
    {
        MFMailComposeViewController *mailPicker = [[MFMailComposeViewController alloc] init];
        mailPicker.mailComposeDelegate = self;
        [mailPicker setSubject:NSLocalizedString(@"新規角ナシリクエスト", @"")];
        [mailPicker setMessageBody:@"本文" isHTML:NO];
        NSArray *recipients = [NSArray arrayWithObjects:@"testflights2015@gmail.com", nil];
        [mailPicker setToRecipients:recipients];
        if ([mailClass canSendMail])
        {
            [self presentViewController:mailPicker animated:TRUE completion:nil];
        }
    }
}

// メール送信処理完了時のイベント
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    switch (result)
    {
            // キャンセル
        case MFMailComposeResultCancelled:
            break;
            // 保存
        case MFMailComposeResultSaved:
            break;
            // 送信
        case MFMailComposeResultSent:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に成功しました。" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
            // 送信失敗
        case MFMailComposeResultFailed:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"送信に失敗しました。" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            break;
        }
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
