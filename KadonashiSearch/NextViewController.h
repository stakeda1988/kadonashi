//
//  NextViewController.h
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "NADView.h"

@interface NextViewController : UIViewController<MFMailComposeViewControllerDelegate>{
    NSString *rowSectionString;
}

@property (strong, nonatomic) NSString *rowSectionString;
- (IBAction)backToTopPage:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;

@end
