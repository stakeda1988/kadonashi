//
//  AppDelegate.h
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    NSNumber *rowSectionNumber;
    NSString *rowSectionString;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSNumber *rowSectionNumber;
@property (strong, nonatomic) NSString *rowSectionString;

@end

