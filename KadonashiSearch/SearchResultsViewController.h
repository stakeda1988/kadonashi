//
//  SearchResultsViewController.h
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NADView.h"

@interface SearchResultsViewController : UIViewController
@property (strong, nonatomic) NSArray *allGrapes;
@property (strong, nonatomic, readonly) NSMutableArray *searchedGrapes;

- (void)query:(NSString *)query;
@end
