//
//  DeviseSize.h
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeviseSize : UIView

//現在起動中のデバイス名の取得
+ (NSString *)getNowDisplayDevice;

@end
