//
//  ViewController.m
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "ViewController.h"
#import "SearchResultsViewController.h"
#import "NextViewController.h"
#import "AppDelegate.h"
#import "DeviseSize.h"

@interface ViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchControllerDelegate, NADViewDelegate >

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *grapes;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) SearchResultsViewController *searchResultsController;
@property (nonatomic, retain) NADView * nadView;
@property (nonatomic) CGFloat intWidth;
@property (nonatomic) CGFloat intHeight;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _intWidth = [[UIScreen mainScreen] bounds].size.width;
    _intHeight = [[UIScreen mainScreen] bounds].size.height;
    
    // (2) NADView の作成
    self.nadView = [[NADView alloc] initWithFrame:CGRectMake(_intWidth/2-160, _intHeight-50, 320, 50)];
    // (3) ログ出力の指定
    [self.nadView setIsOutputLog:NO];
    // (4) set apiKey, spotId.
    [self.nadView setNendID:@"[管理画面より発行された apiKey]"
                     spotID:@"[管理画面より発行された spotID]"];
    [self.nadView setDelegate:self]; //(5)
    [self.nadView load]; //(6)
    [self.view addSubview:self.nadView]; // 最初から表示する場合
    
//    self.canDisplayBannerAds = true;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    _grapes = [NSMutableArray arrayWithCapacity:200];
    [_grapes addObject:@"Hしたくない"];
    [_grapes addObject:@"タバコを吸わないでほしい"];
    [_grapes addObject:@"二人で遊ぶことを誘われたけど、みんな含めてなら遊んでも良い"];
    [_grapes addObject:@"あなたと居ても楽しくない"];
    [_grapes addObject:@"いつも自分のことしか考えてないよね"];
    [_grapes addObject:@"最近奥さんが亡くなった方がいて、娘のように可愛がってくれるのはうれしいけど毎日電話がかかってくるのでやめてほしい"];
    [_grapes addObject:@"打ち上げなんかいってられるか"];
    [_grapes addObject:@"そういう言い方されたら傷つくんですよ"];
    [_grapes addObject:@"なんかいつもプラン立てるの私だけなんだけど少し協力してくれない？"];
    [_grapes addObject:@"会いたくない"];
    [_grapes addObject:@"あなたと一緒にいたら精神的に疲れる"];
    [_grapes addObject:@"どこに住んでるとかプライベートなこと教えたくない"];
    [_grapes addObject:@"辛い・帰りたい・辞めたい・面倒臭い"];
    [_grapes addObject:@"早く帰りたい"];
    [_grapes addObject:@"なんか重くてしんどくなってきた"];
    [_grapes addObject:@"後輩からいきなりタメ語で話された"];
    [_grapes addObject:@"好きな人が自分の嫌いな人とばっかりしゃべって自分にかまってくれない"];
    [_grapes addObject:@"あんまり関わりたくない友達に毎月「シフト出た？シフト表送って！遊ぼう!」って催促される"];
    [_grapes addObject:@"担任が体を触ってくる"];
    [_grapes addObject:@"気が使えないね"];
    [_grapes addObject:@"食べ方汚い！"];
    [_grapes addObject:@"もう帰りたい"];
    [_grapes addObject:@"うるさい"];
    [_grapes addObject:@"「普通」◯◯でしょ？"];
    [_grapes addObject:@"生理的に無理"];
    [_grapes addObject:@"顔、怖い！"];
    [_grapes addObject:@"チームワークがなってなくて足手まとい"];
    [_grapes addObject:@"マナーがなってない"];
    [_grapes addObject:@"全体的にキツイ"];
    [_grapes addObject:@"ほんとに無理"];
    [_grapes addObject:@"なんか臭いんだけど"];
    [_grapes addObject:@"今日はめんどくさい"];
    [_grapes addObject:@"上司のいいなりすぎて犬みたい"];
    [_grapes addObject:@"おまえ、風呂入れよ！"];
    [_grapes addObject:@"飲み物、食べ物をあげたくない(関節キスが嫌)とき"];
    [_grapes addObject:@"めっちゃLINEが長く続いてて、もうやめたい"];
    [_grapes addObject:@"すんげえきらい"];
    [_grapes addObject:@"食後相手の歯にのりとかがついてる時は？"];
    [_grapes addObject:@"おもしろくない！！さむい！！"];
    [_grapes addObject:@"しったかぶってくるのうざいよ"];
    [_grapes addObject:@"好きです"];
    [_grapes addObject:@"人に色々言う割には自分もあまり(仕事が)できてないよね！！"];
    [_grapes addObject:@"うざい"];
    [_grapes addObject:@"人としてダメ"];
    [_grapes addObject:@"性欲高すぎ"];
    [_grapes addObject:@"汚されそうだしパクられそうだから、漫画貸したくない"];
    [_grapes addObject:@"話がやたら長い"];
    [_grapes addObject:@"マザコンきめえ"];
    [_grapes addObject:@"鼻毛でてるよ！"];
    [_grapes addObject:@"自慢話ばっかりでうんざり"];
    [_grapes addObject:@"口、くさい"];
    [_grapes addObject:@"調子乗るな"];
    [_grapes addObject:@"ただの遊びなんだけど"];
    [_grapes addObject:@"お金の使い方が下手すぎ"];
    [_grapes addObject:@"気づいてないと思うけど、あなた結構嫌われてるよ"];
    [_grapes addObject:@"早漏すぎだろお前"];
    [_grapes addObject:@"家に入れたくない"];
    [_grapes addObject:@"死んだほうがマシだろ"];
    [_grapes addObject:@"誘いに対して、正直乗り気じゃない"];
    [_grapes addObject:@"恋愛対象では無くなった"];
    [_grapes addObject:@"友達の鼻くそが、誰が見ても見えるくらいでてる"];
    [_grapes addObject:@"あの子とじゃなくて私とデートして！"];
    [_grapes addObject:@"異性に連絡先を聞かれた時の断り方"];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _searchResultsController = [sb instantiateViewControllerWithIdentifier:@"SearchResultsController"];
    _searchResultsController.allGrapes = _grapes;
    _searchController = [[UISearchController alloc] initWithSearchResultsController:_searchResultsController];
    _searchController.searchBar.frame = CGRectMake(0, 0, self.view.bounds.size.width, 44);
    NSString *deviceName = [DeviseSize getNowDisplayDevice];
    //iPhone4s
//    if([deviceName isEqual:@"iPhone4s"]){
//        _searchController.searchBar.frame = CGRectMake(0, 0, 310, 44);
//    //iPhone5またはiPhone5s
//    }else if ([deviceName isEqual:@"iPhone5"]){
//        _searchController.searchBar.frame = CGRectMake(0, 0, 310, 44);
//    //iPhone6
//    }else if ([deviceName isEqual:@"iPhone6"]){
//        _searchController.searchBar.frame = CGRectMake(0, 0, 375, 44);
//    //iPhone6 plus
//    }else if ([deviceName isEqual:@"iPhone6plus"]){
//        _searchController.searchBar.frame = CGRectMake(0, 0, 414, 44);
//    }
    _searchController.searchBar.returnKeyType = UIReturnKeyDone;
    _searchController.searchBar.placeholder = @"検索キーワード";
    _searchController.searchBar.backgroundColor = [UIColor clearColor];
    _searchController.searchBar.delegate = self;
    _searchController.delegate = self;
    
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    _tableView.dataSource = self;
    _tableView.tableHeaderView = _searchController.searchBar;
    _tableView.rowHeight = 80;
    _tableView.backgroundView = nil;
    [_tableView setBackgroundColor: [UIColor clearColor]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _grapes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *row = [_grapes objectAtIndex:indexPath.row];
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.text = row;
    cell.textLabel.font = [UIFont fontWithName:@"07LogoTypeGothic7" size:15.0];
    cell.backgroundColor = [UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_searchResultsController query:searchText];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NextViewController
    *weightVC1 = [[NextViewController alloc] init];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    weightVC1.rowSectionString = cell.textLabel.text;
    weightVC1.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self presentViewController:weightVC1 animated:YES completion: nil];
}

@end
