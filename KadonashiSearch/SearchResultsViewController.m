//
//  SearchResultsViewController.m
//  KadonashiSearch
//
//  Created by SHOKI TAKEDA on 8/23/15.
//  Copyright (c) 2015 misakishiki.com. All rights reserved.
//

#import "SearchResultsViewController.h"
#import "ViewController.h"
#import "NextViewController.h"
#import "AppDelegate.h"

@interface SearchResultsViewController () <UITableViewDataSource, NADViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NADView * nadView;
@end

@implementation SearchResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
//    self.canDisplayBannerAds = true;
    
    _searchedGrapes = [NSMutableArray arrayWithCapacity:20];
    
    [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    _tableView.dataSource = self;
    _tableView.backgroundView = nil;
    [_tableView setBackgroundColor: [UIColor clearColor]];
}

- (void)query:(NSString *)query {
    [_searchedGrapes removeAllObjects];
    for (NSString *grape in _allGrapes) {
        NSRange range = [grape rangeOfString:query];
        if (range.location != NSNotFound) {
            [_searchedGrapes addObject:grape];
        }
    }
    [_tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _searchedGrapes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *row = [_searchedGrapes objectAtIndex:indexPath.row];
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = row;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:@"07LogoTypeGothic7" size:15.0];
    cell.backgroundColor = [UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NextViewController
    *weightVC1 = [[NextViewController alloc] init];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    weightVC1.rowSectionString = cell.textLabel.text;
    weightVC1.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self presentViewController:weightVC1 animated:YES completion: nil];
}

@end
